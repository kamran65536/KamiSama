﻿global using KamiSama.Extensions.DependencyInjection;
global using KamiSama.Extensions.Paging;
global using KamiSama.Repositories.EntityFrameworkCore;
global using Example.Core.Entities;
global using Example.Repositories.Abstractions;
global using Example.Repositories.SearchModels;
global using Microsoft.EntityFrameworkCore;
global using Microsoft.EntityFrameworkCore.Metadata.Builders;
global using Microsoft.Extensions.DependencyInjection;
global using System.Diagnostics.CodeAnalysis;

[assembly: ExcludeFromCodeCoverage]