﻿global using KamiSama.Extensions.DependencyInjection.Tests.DataTypes;
global using Microsoft.Extensions.DependencyInjection;
global using Microsoft.VisualStudio.TestTools.UnitTesting;
global using System;
global using System.Linq;
global using NSubstitute;
global using System.Reflection;

using System.Diagnostics.CodeAnalysis;

[assembly: ExcludeFromCodeCoverage]