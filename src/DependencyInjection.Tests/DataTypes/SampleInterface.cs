﻿namespace KamiSama.Extensions.DependencyInjection.Tests.DataTypes;

public interface ISampleInterface
{
	int X { get; set; }
}
