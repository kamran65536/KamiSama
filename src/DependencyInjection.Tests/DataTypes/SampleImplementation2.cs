﻿using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Extensions.DependencyInjection.Tests.DataTypes;

public class SampleImplementation2 : ISampleInterface
{
	public int X { get; set; }
}
