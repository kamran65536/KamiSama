﻿global using Microsoft.Extensions.DependencyInjection;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("KamiSama.Extensions.DependencyInjection.Tests")]