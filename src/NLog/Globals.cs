﻿global using NLog;
global using NLog.Config;
global using NLog.LayoutRenderers;
global using System.Globalization;
global using System.Text;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("KamiSama.Extensions.Nlog.Tests")]