﻿global using System.Text.Json;
global using System.Text.Json.Serialization;

using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("KamiSama.Extensions.Json.Tests")]