﻿global using KamiSama.Extensions.Paging;
global using KamiSama.Repositories.Json;
global using Microsoft.VisualStudio.TestTools.UnitTesting;
global using NSubstitute;
global using System;
global using System.Collections.Generic;
global using System.IO;
global using System.Linq;
global using System.Linq.Expressions;
global using System.Threading;
global using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;

[assembly: ExcludeFromCodeCoverage]