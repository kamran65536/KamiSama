﻿global using Microsoft.VisualStudio.TestTools.UnitTesting;
global using System.Numerics;
global using System.Text.Json;
global using System.Text.Json.Serialization;
global using System;
global using System.Linq;

using System.Diagnostics.CodeAnalysis;

[assembly: ExcludeFromCodeCoverage]