﻿global using AutoMapper;
global using KamiSama.Chassis.Commons.Repositories;
global using KamiSama.Chassis.HttpExceptions;
global using KamiSama.Extensions.Paging;
global using KamiSama.Repositories.EntityFrameworkCore;
global using Microsoft.EntityFrameworkCore;

