# KamiSama Extension Libraries
## Introduction
KamiSama.Extension libraries are set of suger codes and helper methods for most common usages.
Here is the list of libraries:
1. [KamiSama.Extensions.Core](./docs/1-core.md)
    some core extension methods for arrays, strings, enumerables, datetime, ...
2. [KamiSama.Extensions.DependencyInjection](./docs/2-dependency-injection.md)
    addtional functionality to Microsoft.Extensions.DependencyInjection, such as named injection
3. [KamiSama.Extensions.Json](./docs/3-json.md)
    useful converters and extensions such ExpandoObjectConverter, HexByteArrayConverter, HierarchicalJsonConverter,...
4. [KamiSama.Extensions.Xml](./docs/4-xml.md)
    additional attributes for Xml Serialization such as AutoXmlAttributes to automatically add inherited classes to parent
5. [KamiSama.Extensions.Repositories libraries](./docs/5-repositories.md)
    repository patten default implementation for EntityFramework and EntityFrameworkCore.
6. [KamiSama.Extensions.NLog](./docs/6-nlog.md)
    NLog extensions for encryption archieving and persian fast log renderer
7. [KamiSama.IranSystem](./docs/7-iran-system.md)
    Iran System Encoding used in many old Persian applications.
8. [KamiSama.Chassis](./docs/8-chassis.md)
    Chassis for less code in AspNetCore web applications. eg. exception handling
9. [KamiSama.Testing.NSubstitute](../docs/9-testings.md)
    helper class to write more cleaner and comprehensible tests using dependency injection.
10. [KamiSama.Templates](../docs/10-templates.md)
    Template project for most common scenarios like Core (Domain) project, BoundedContext projects, Repository projects...
11. [KamiSama.Extensions.WebModules](../docs/11-web-modules.md)
    Web Modules are the a way to simplify the process of building web applications and web apis.