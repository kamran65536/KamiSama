# 6. KamiSama.IranSystem Library
IranSystem is an old ASCII extension encoding used in many old dos programs. 
The original code was from (https://www.codeproject.com/Tips/272928/iranian-system-encoding-standard).

:warning: **As IranSystem is a shape based font, the conversion will not be the same as usual ascii codes. so becareful!**